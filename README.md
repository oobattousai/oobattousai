# Matheus

Hi, welcome to my profile. I'm devops enginner

## :hammer_and_wrench: Languages and Tools :

* PHP (zend framework 1, laravel)
* Git (Gitlab, gitlab-ci, github, github actions)
* Docker (docker compose)
* Javascript (nodejs, vuejs, typescript)
* Python
* Go
* Terraform
* AWS
* Ansible
* Linux user